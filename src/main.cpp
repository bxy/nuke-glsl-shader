// GLSL shader node for Nuke 10.5

// Use the Windows 7 SDK MSVC 2010 x64 compiler

#define MAIN

static const char* const CLASS = "NukeGLSL";
static const char* const HELP = "Nuke GLSL shader execution node";

// Nuke SDK includes
#include "DDImage/Op.h"
#include "DDImage/Iop.h"
#include "DDImage/DrawIop.h"
#include "DDImage/PixelIop.h"
#include "DDImage/Black.h"

#include "DDImage/Knobs.h"
#include "DDImage/DDMath.h"

#include "DDImage/Knob.h"
#include "DDImage/Format.h"
#include "DDImage/Channel3D.h"
#include "DDImage/Interest.h"
#include "DDImage/Row.h"
#include "DDImage/Tile.h"
#include "DDImage/ChannelSet.h"
#include "DDImage/Hash.h"
#include "DDImage/NukeWrapper.h"

#include "DDImage/DDImage_API.h"

// General includes
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <iostream>
#include <assert.h>
#include <fstream>
#include <ctime>
#include <algorithm>

// OpenGL related includes
#include <GL/glew.h>
#include <GLFW/glfw3.h>

#ifdef _WIN32
#pragma comment(lib, "opengl32")
#endif

#include "framebuffer.h"
#include "framebufferlayer.h"

#define MAX_INPUTS 4


class NukeGLSL : public DD::Image::Iop
{
private:
    // Knob stuff
    DD::Image::FormatPair formats;
    bool use_input0_format;
    const char* filename;
    bool usetime;
    bool debug;
    const char* vertexShaderCode;
    const char* fragmentShaderCode;
    float frameRate;
    float mouseposition[2];
    int numIterations;

    // Input and output framebuffer related stuff
    std::vector<FrameBuffer*> inputFrameBuffers;
    FrameBuffer* outputFrameBuffer;
    FrameBuffer* intermediateFrameBuffer;

    // OpenGL related data
    GLFWwindow* window;
    GLuint programID;
    GLuint vertexArrayID;
    GLuint vertexBuffer;

    // General housekeeping
    bool openCalled;
    bool windowCreated;
    bool shaderCompiled;


public:
    int maximum_inputs() const { return MAX_INPUTS; }
    int minimum_inputs() const { return 1; }

    void _validate(bool);
    void _request(int x, int y, int r, int t, DD::Image::ChannelMask channels, int count);
    void _open();
    void engine ( int y, int x, int r, DD::Image::ChannelMask channels, DD::Image::Row& out );

    void append(DD::Image::Hash& hash);

    // make sure that all members are initialized
    NukeGLSL(Node* node) : Iop(node)
    {
        use_input0_format = false;
        filename = "";
        usetime = true;
        debug = false;
        frameRate = 24.0f;
        mouseposition[0] = mouseposition[1] = 0.0f;
        numIterations = 1;

        vertexShaderCode = "void main(){\n"
                           "    vec3 pos = gl_Vertex.xyz;"
                           "    gl_Position = vec4(pos, 1.0);\n"
                           "};\n";

        fragmentShaderCode = "#version 120\n"
                             "uniform float iTime;\n"
                             "uniform vec2 iResolution;\n"
                             "void main(){\n"
                             "    gl_FragColor = vec4(1, 0, 0, 1);\n"
                             "};\n";


        outputFrameBuffer = nullptr;
        intermediateFrameBuffer = nullptr;
        window = NULL;
        programID = 0;
        vertexArrayID = 0;
        vertexBuffer = 0;

        openCalled = false;
        windowCreated = false;
        shaderCompiled = false;
    }

    virtual void knobs(DD::Image::Knob_Callback);
    int knob_changed(DD::Image::Knob* k);
    const char* input_label(int input, char* buffer) const;

    const char* Class() const { return CLASS; }
    const char* node_help() const { return HELP; }
    const char* displayName() const { return "NukeGLSL"; }
    static const Iop::Description description;

    void logMessage(std::string logMessage);
    bool inputUsedInShader(int input);
    void clearFrameBuffers();
    bool createWindow();
    void changeResolution();
    GLuint loadShadersFromCode(const char* vertex_shader_code, const char* fragment_shader_code);
    bool compileShader();
    bool drawToWindow(int currentIteration);
    void terminateGL();
};



void NukeGLSL::knobs(DD::Image::Knob_Callback f)
{
    DD::Image::Format_knob(f, &formats, "format");
    DD::Image::Bool_knob(f, &use_input0_format, "use_input0_format", "Use format of input 0");
    DD::Image::Bool_knob(f, &debug, "debug", "Debug logging");
    DD::Image::SetFlags(f, DD::Image::Knob::STARTLINE);
    DD::Image::File_knob(f, &filename, "shader_file", "Shader file");
    Divider(f);
    DD::Image::Multiline_String_knob(f, &fragmentShaderCode, "fragment_shader_code", "Fragment Shader", 100);
    Divider(f);
    DD::Image::Float_knob(f, &frameRate, "framerate", "Frame rate");
    DD::Image::XY_knob(f, mouseposition, "mouseposition", "Mouse position");
    DD::Image::Int_knob(f, &numIterations, "numiterations", "Number of iterations");
}


int NukeGLSL::knob_changed(DD::Image::Knob* k)
{
    if(k->is("shader_file"))
    {
        return true;
    }

    if(k->is("fragment_shader_code"))
    {
        return true;
    }

    return DD::Image::Iop::knob_changed(k);
}


const char* NukeGLSL::input_label(int input, char* buffer) const
{
    switch (input)
    {
        case 0:
            return "iTexture0";
        case 1:
            return "iTexture1";
        case 2:
            return "iTexture2";
        case 3:
            return "iTexture3";
        default:
            return "iTexture";
    }
}


// Generic helper logger
void NukeGLSL::logMessage(std::string message)
{
    if (debug)
    {
        std::cout << message << std::endl;
    }
}


// Validate calls validate on first input and if so set,
// updates the node format and recreates OpenGL context.
// Nothing else happens here to keep it fast

void NukeGLSL::_validate(bool for_real)
{
    // Validate all inputs
    for (int i = 0; i < MAX_INPUTS; ++i)
    {
        if (input(i))
        {
          input(i)->validate(for_real);
        }
    }


    int newWidth, newHeight;

    // Create format based on format combobox.
    // If we have use first input checkbox checked, derive format from that.
    // Something is currently messed up with bounding box size
    if (use_input0_format)
    {
        copy_info();
    } else {
        info_.full_size_format(*formats.fullSizeFormat());
        info_.format(*formats.format());
        info_.channels(DD::Image::Mask_RGBA);
        info_.set(format());

        DD::Image::Format curFormat = *formats.fullSizeFormat();
        newWidth = curFormat.width();
        newHeight = curFormat.height();
    }

    // If we don't have framebuffer yet, create it
    if (!outputFrameBuffer)
    {
        outputFrameBuffer = new FrameBuffer();
        intermediateFrameBuffer = new FrameBuffer();
    }


    // If size has changed, change buffer sizes and recreate window and context
    if (input(0))
    {
        // If we want to use input format, use size from that
        if (use_input0_format)
        {
            newWidth = input(0)->format().width();
            newHeight = input(0)->format().height();
        }

        if (newWidth != outputFrameBuffer->width() || newHeight != outputFrameBuffer->height())
        {
            outputFrameBuffer->setSize(newWidth, newHeight);
            outputFrameBuffer->setContext(window);
            outputFrameBuffer->createTexture();
            outputFrameBuffer->setInitialized(true);

            intermediateFrameBuffer->setSize(newWidth, newHeight);
            intermediateFrameBuffer->setContext(window);
            intermediateFrameBuffer->createTexture();
            intermediateFrameBuffer->setInitialized(true);

            changeResolution();
        }
    }

    openCalled = false;
}


void NukeGLSL::_request(int x, int y, int r, int t, DD::Image::ChannelMask channels, int count)
{
    for (int i = 0; i < MAX_INPUTS; ++i)
    {
        if (input(i))
        {
          const DD::Image::Box& b = input(i)->info();
          input(i)->request(b.x(), b.y(), b.r(), b.t(), DD::Image::Mask_RGBA, count);
        }
    }
}


template <typename T> std::string tostr(const T& t) {
   std::ostringstream os;
   os << t;
   return os.str();
}


// Here we read data from input textures and copy data to OpenGL texture buffers
void NukeGLSL::_open()
{
    if (openCalled)
        return;

    // Clear all data first
    clearFrameBuffers();
    inputFrameBuffers.resize(MAX_INPUTS);

    // Start timer to time memory operations
    clock_t begin_time = clock();

    // Iterate over inputs
    for (int i = 0; i < MAX_INPUTS; ++i)
    {
        // New framebuffer object that will hold input texture data
        // It is created before checks to have a vector with size of inputs
        logMessage("Input number: " + tostr(i));

        // Check that input is connected and that input is used in shader at all
        Iop* curInput = input(i);
        if (!curInput || !inputUsedInShader(i))
            continue;

        // Only create actual buffer if input is connected and used
        FrameBuffer* curFrameBuffer = new FrameBuffer();
        inputFrameBuffers.at(i) = curFrameBuffer;

        int width, height;
        width = curInput->format().width();
        height = curInput->format().height();
        U64 hash = curInput->hash().value();
        logMessage("Width: " + tostr(width));
        logMessage("Height: " + tostr(height));
        logMessage("Hash: " + tostr(hash));

        bool disconnected = false;
        if (dynamic_cast<DD::Image::Black*>(input(i)))
        {
            disconnected = true;
            logMessage("Input disconnected");
        }

        // Context MUST be set before initialization
        curFrameBuffer->setContext(window);
        curFrameBuffer->setSize(width, height);
        curFrameBuffer->createTexture();
        curFrameBuffer->setName("FB_" + tostr(i));
        curFrameBuffer->setHash(hash);
        curFrameBuffer->setLayerCount(4);
        curFrameBuffer->setInitialized(true);

        //Channels from input and RGBA channel mask to intersect with
        DD::Image::ChannelSet chmask, inputChannels;
        chmask += DD::Image::Mask_RGBA;
        inputChannels = curInput->channels();

        // For reading we want to access the input format, not bbox.
        DD::Image::Format format = curInput->format();
        const int fx = format.x();
        const int fy = format.y();
        const int fr = format.r();
        const int ft = format.t();

        // Lock an area into the cache using an interest and release immediately
        // Why it is done this way, beats me, but example scripts do this also
        DD::Image::Interest interest(*curInput, fx, fy, fr, ft, chmask, true);
        interest.unlock();

        foreach(channel, chmask)
        {
            // New input layer set that will be filled with data
            FrameBufferLayer* curLayer = new FrameBufferLayer();
            curFrameBuffer->addLayer(curLayer, channel - 1);    // -1 is because channel 0 is black and we want index to start at 0 with red

            // Intersect channel with input channels
            if (inputChannels.contains(channel))
            {
                curLayer->setChannel(channel);
                curLayer->initialize(width, height);

                if (curLayer->allocatedSize() < width*height)
                {
                    logMessage("Allocated too little memory in framebuffer layer");
                    continue;
                }

                // Iterate over rows and copy contents to buffer
                // Check if disconnected, not necessary to copy anything if is
                if (!disconnected)
                {
                    int currentRow = 0;
                    for (int ry = fy; ry < ft; ry++)
                    {
                        DD::Image::Row row(fx, fr);
                        row.get(*curInput, ry, fx, fr, chmask);

                        const float *CUR = row[channel] + fx;
                        const int rowPtr = currentRow * width;
                        float *dst = &curLayer->data()[rowPtr];
                        memcpy(dst, CUR, sizeof(float) * width);
                        currentRow++;
                    }

                    // This debug part is just for probing values in buffers
                    if (debug)
                    {
                        int index = mouseposition[1] * curLayer->width() + mouseposition[0];
                        float value = curLayer->data()[index];
                        logMessage("Framebufferlayer value: " + tostr(value));
                    }
                }

            } // channel intersect
        } // foreach

        // Now that framebuffer is filled, copy data to gpu for use.
        // Check for disconnected because if there is no input (meaning black image) there is nothing to copy
        if (!disconnected)
            curFrameBuffer->copyDataToGPU();
    } // inputs

    std::cout << "Inputs copy time: " << float( clock () - begin_time ) /  CLOCKS_PER_SEC << std::endl;

    // If we have an opengl window, draw to it
    if (windowCreated)
    {
        begin_time = clock();

        compileShader();
        drawToWindow(numIterations);

        // We run the drawing loop for specified number of times
//        for (int i = 0; i < numIterations; ++i)
//        {
//            drawToWindow(i);
//            std::cout << "Iteration number: " << i + 1 << "/" << numIterations << std::endl;
//        }

        std::cout << "Compile-Bind-Draw time: " << float( clock () - begin_time ) /  CLOCKS_PER_SEC << std::endl;
    }

    openCalled = true;
}



void NukeGLSL::append(DD::Image::Hash& hash)
{
    // If we have a shader that is animated, append frame to hash
    if (usetime)
        hash.append(outputContext().frame());
}



// This is a helper for cleaning up all input framebuffers
// Each framebuffer is responsible for cleaning up its stuff, including memory

void NukeGLSL::clearFrameBuffers()
{
    for (int i = 0; i < inputFrameBuffers.size(); ++i)
    {
        FrameBuffer* fb = inputFrameBuffers.at(i);
        if (fb)
        {
            fb->deleteAll();
            delete fb;
        }
    }
    inputFrameBuffers.clear();
}



void NukeGLSL::changeResolution()
{
    terminateGL();
    createWindow();
}


void NukeGLSL::terminateGL()
{
    // Close OpenGL window and terminate GLFW
    glfwTerminate();
    windowCreated = false;
}



bool NukeGLSL::createWindow()
{
    windowCreated = false;

    const int windowWidth = outputFrameBuffer->width();
    const int windowHeight = outputFrameBuffer->height();

    if( !glfwInit() )
    {
        fprintf( stderr, "Failed to initialize GLFW\n" );
        return false;
    }

    glfwWindowHint(GLFW_SAMPLES, 4);

    // Version must be 2.1, otherwise Nuke viewer will go crazy
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 2);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 1);

    // Invisible window, because we just need it for context
    glfwWindowHint(GLFW_VISIBLE, GLFW_FALSE);

    // This should force full float processing, but output is still clipped...
    glfwWindowHint(GLFW_RED_BITS, 32);
    glfwWindowHint(GLFW_GREEN_BITS, 32);
    glfwWindowHint(GLFW_BLUE_BITS, 32);
    glfwWindowHint(GLFW_ALPHA_BITS, 32);

    // Open a window and create its OpenGL context
    window = glfwCreateWindow( windowWidth, windowHeight, "Nuke GLSL", NULL, NULL);
    if( window == NULL ){
        fprintf( stderr, "Failed to open GLFW window.\n" );
        glfwTerminate();
        return false;
    }

    glfwMakeContextCurrent(window); // Initialize GLEW

    if (glewInit() != GLEW_OK) {
        fprintf(stderr, "Failed to initialize GLEW\n");
        window = NULL;
        glfwMakeContextCurrent(NULL);
        glfwTerminate();
        return false;
    }


    // This is the actual screen surface we draw on. A simple quad.
    glGenVertexArrays(1, &vertexArrayID);
    glBindVertexArray(vertexArrayID);

    static const GLfloat g_vertex_buffer_data[] = {
        -1.0f, -1.0f, 0.0f,
         1.0f, -1.0f, 0.0f,
         1.0f,  1.0f, 0.0f,
        -1.0f,  1.0f, 0.0f,
    };

    glGenBuffers(1, &vertexBuffer);
    glBindBuffer(GL_ARRAY_BUFFER, vertexBuffer);
    glBufferData(GL_ARRAY_BUFFER, sizeof(g_vertex_buffer_data), g_vertex_buffer_data, GL_STATIC_DRAW);

    glfwMakeContextCurrent(NULL);

    windowCreated = true;
    return windowCreated;
}


GLuint NukeGLSL::loadShadersFromCode(const char* vertex_shader_code, const char* fragment_shader_code)
{

    // Create the shaders
    GLuint VertexShaderID = glCreateShader(GL_VERTEX_SHADER);
    GLuint FragmentShaderID = glCreateShader(GL_FRAGMENT_SHADER);

    GLint Result = GL_FALSE;
    int InfoLogLength;

    // Compile Vertex Shader
    char const * VertexSourcePointer = vertex_shader_code;
    glShaderSource(VertexShaderID, 1, &VertexSourcePointer , NULL);
    glCompileShader(VertexShaderID);

    // Check Vertex Shader
    glGetShaderiv(VertexShaderID, GL_COMPILE_STATUS, &Result);
    glGetShaderiv(VertexShaderID, GL_INFO_LOG_LENGTH, &InfoLogLength);
    if ( InfoLogLength > 0 ){
        std::vector<char> VertexShaderErrorMessage(InfoLogLength+1);
        glGetShaderInfoLog(VertexShaderID, InfoLogLength, NULL, &VertexShaderErrorMessage[0]);
        printf("%s\n", &VertexShaderErrorMessage[0]);
    }

    // Compile Fragment Shader
    char const * FragmentSourcePointer = fragment_shader_code;
    glShaderSource(FragmentShaderID, 1, &FragmentSourcePointer , NULL);
    glCompileShader(FragmentShaderID);

    // Check Fragment Shader
    glGetShaderiv(FragmentShaderID, GL_COMPILE_STATUS, &Result);
    glGetShaderiv(FragmentShaderID, GL_INFO_LOG_LENGTH, &InfoLogLength);
    if ( InfoLogLength > 0 ){
        std::vector<char> FragmentShaderErrorMessage(InfoLogLength+1);
        glGetShaderInfoLog(FragmentShaderID, InfoLogLength, NULL, &FragmentShaderErrorMessage[0]);
        printf("%s\n", &FragmentShaderErrorMessage[0]);
    }

    // Link the program
    GLuint ProgramID = glCreateProgram();
    glAttachShader(ProgramID, VertexShaderID);
    glAttachShader(ProgramID, FragmentShaderID);
    glLinkProgram(ProgramID);

    // Check the program
    glGetProgramiv(ProgramID, GL_LINK_STATUS, &Result);
    glGetProgramiv(ProgramID, GL_INFO_LOG_LENGTH, &InfoLogLength);
    if ( InfoLogLength > 0 ){
        std::vector<char> ProgramErrorMessage(InfoLogLength+1);
        glGetProgramInfoLog(ProgramID, InfoLogLength, NULL, &ProgramErrorMessage[0]);
        printf("%s\n", &ProgramErrorMessage[0]);
    }

    glDetachShader(ProgramID, VertexShaderID);
    glDetachShader(ProgramID, FragmentShaderID);

    glDeleteShader(VertexShaderID);
    glDeleteShader(FragmentShaderID);

    return ProgramID;
}


bool NukeGLSL::compileShader()
{
    shaderCompiled = false;

    glfwMakeContextCurrent(window);

    // Create shader program
    if (programID)
        glDeleteProgram(programID);

    programID = loadShadersFromCode(vertexShaderCode, fragmentShaderCode);
    if (programID)
        shaderCompiled = true;

    glfwMakeContextCurrent(NULL);

    return shaderCompiled;
}



// This is a stomp for checking if some input is actually used in shader
// If it is not, no reason to copy input data to GPU
bool NukeGLSL::inputUsedInShader(int input)
{
    return true;
}



bool NukeGLSL::drawToWindow(int currentIteration)
{

    glfwMakeContextCurrent(window);

    glClearColor(1.0f, 0.0f, 0.0f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    // Not sure which flags here are actually needed
    glEnable(GL_TEXTURE_2D);
    glDisable(GL_CULL_FACE);
    glDepthMask(GL_FALSE);      // disable writes to Z-Buffer
    glDisable(GL_DEPTH_TEST);   // disable depth-testing

    // Use our shader
    glUseProgram(programID);

    // Uniform stuff
    // First set up lists of possible uniform names for each parameter
    // Later use these names to probe shader for uniforms
    // Different shaders can have different names, for example Shadertoy
    // uses iTime, iFrame etc

    const char* iterUniformArgs[] = {"iIteration", "Iteration", "iteration"};
    std::vector<std::string> iterUniforms(iterUniformArgs, iterUniformArgs + 3);
    const char* iterCountUniformArgs[] = {"iIterationCount", "IterationCount", "iterationcount"};
    std::vector<std::string> iterCountUniforms(iterCountUniformArgs, iterCountUniformArgs + 3);
    const char* frameUniformArgs[] = {"iFrame", "Frame", "frame"};
    std::vector<std::string> frameUniforms(frameUniformArgs, frameUniformArgs + 3);
    const char* timeUniformArgs[] = {"iTime", "Time", "time"};
    std::vector<std::string> timeUniforms(timeUniformArgs, timeUniformArgs + 3);
    const char* mouseUniformArgs[] = {"iMouse", "Mouse", "mouse"};
    std::vector<std::string> mouseUniforms(mouseUniformArgs, mouseUniformArgs + 3);
    const char* resolutionUniformArgs[] = {"iResolution", "Resolution", "resolution"};
    std::vector<std::string> resolutionUniforms(resolutionUniformArgs, resolutionUniformArgs + 3);
    const char* textureUniformArgs[] = {"iTexture", "Texture", "texture", "iChannel"};
    std::vector<std::string> textureUniforms(textureUniformArgs, textureUniformArgs + 4);
    const char* resolutionSelfUniformArgs[] = {"iResolutionSelf", "ResolutionSelf", "resolutionself"};
    std::vector<std::string> resolutionSelfUniforms(resolutionSelfUniformArgs, resolutionSelfUniformArgs + 3);
    const char* feedbackTextureUniformArgs[] = {"iFeedback", "Feedback", "feedback"};
    std::vector<std::string> feedbackTextureUniforms(feedbackTextureUniformArgs, feedbackTextureUniformArgs + 3);

    GLsizei outputWidth = outputFrameBuffer->width();
    GLsizei outputHeight = outputFrameBuffer->height();
    float currentFrame = outputContext().frame();
    float currentTime = currentFrame / frameRate;
    float mousePos[2] = { mouseposition[0] / outputFrameBuffer->width(), mouseposition[1] / outputFrameBuffer->height() };

    // General input data uniforms
    GLint generalLocation;

    // Iteration uniforms, this feeds the iteration count to shader so we know what the max count is
    for (int i = 0; i < iterCountUniforms.size(); ++i)
    {
        std::string uniformName = iterCountUniforms.at(i);
        generalLocation = glGetUniformLocation(programID, uniformName.c_str());
        glUniform1f(generalLocation, numIterations);
    }

    // Frame uniforms
    for (int i = 0; i < frameUniforms.size(); ++i)
    {
        std::string uniformName = frameUniforms.at(i);
        generalLocation = glGetUniformLocation(programID, uniformName.c_str());
        glUniform1f(generalLocation, currentFrame);
    }

    // Time uniforms
    for (int i = 0; i < timeUniforms.size(); ++i)
    {
        std::string uniformName = timeUniforms.at(i);
        generalLocation = glGetUniformLocation(programID, uniformName.c_str());
        glUniform1f(generalLocation, currentTime);
    }

    // Mouse uniforms
    for (int i = 0; i < mouseUniforms.size(); ++i)
    {
        std::string uniformName = mouseUniforms.at(i);
        generalLocation = glGetUniformLocation(programID, uniformName.c_str());
        glUniform2fv(generalLocation, 1, mousePos);
    }


    // Texture uniforms
    GLenum texBaseAddress = GL_TEXTURE0;
    GLuint TextureLocations[MAX_INPUTS] = {0};
    GLint ResolutionLocations[MAX_INPUTS] = {0};
    for (int i = 0; i < inputFrameBuffers.size(); ++i)
    {
        FrameBuffer* fb = inputFrameBuffers.at(i);
        if (fb && fb->initialized())
        {
            // Bind texture uniforms to texture name
            for (int u = 0; u < textureUniforms.size(); ++u)
            {
                std::string textureName = textureUniforms.at(u) + tostr(i);
                TextureLocations[i] = glGetUniformLocation(programID, textureName.c_str());
                glActiveTexture(texBaseAddress + i);
                glBindTexture(GL_TEXTURE_2D, fb->texture());
                glUniform1i(TextureLocations[i], i);
                //std::cout << "Framebuffer " << i << " location in shader: " << TextureLocations[i] << std::endl;

                // A general case with no index from first input that some shaders use
                if (i == 0)
                {
                    textureName = textureUniforms.at(u);
                    TextureLocations[i] = glGetUniformLocation(programID, textureName.c_str());
                    glActiveTexture(texBaseAddress + i);
                    glBindTexture(GL_TEXTURE_2D, fb->texture());
                    glUniform1i(TextureLocations[i], i);
                }
            }

            // Bind texture resolutions to uniforms
#ifdef _WIN32
            float bufferResolution[2] = {fb->width(), fb->height()};
#else
            float bufferResolution[2] = {fb->float(width()), fb->float(height())};
#endif


            for (int u = 0; u < resolutionUniforms.size(); ++u)
            {
                std::string resolutionName = resolutionUniforms.at(u) + tostr(i);
                ResolutionLocations[i] = glGetUniformLocation(programID, resolutionName.c_str());
                glUniform2fv(ResolutionLocations[i], 1, bufferResolution);

                // A general case with no index from first input that some shaders use
                if (i == 0)
                {
                    resolutionName = resolutionUniforms.at(u);
                    ResolutionLocations[i] = glGetUniformLocation(programID, resolutionName.c_str());
                    glUniform2fv(ResolutionLocations[i], 1, bufferResolution);
                }
            }
        }
    }


    // Here we bind the output buffer to special input uniform called feedbackTexture
    // This mechanism makes the output accessible when doing multiple iterations
    // Texture uniforms
    GLenum feedbackTexBaseAddress = GL_TEXTURE8;
    GLuint feedbackTextureLocation = 0;
    GLint ResolutionLocation = 0;
    FrameBuffer* fb = intermediateFrameBuffer;
    if (fb)
    {
        for (int i = 0; i < feedbackTextureUniforms.size(); ++i)
        {
            std::string uniformName = feedbackTextureUniforms.at(i);
            feedbackTextureLocation = glGetUniformLocation(programID, uniformName.c_str());
            glActiveTexture(feedbackTexBaseAddress);
            glBindTexture(GL_TEXTURE_2D, fb->texture());
            glUniform1i(feedbackTextureLocation, 8);
            //std::cout << "Feedback location in shader: " << feedbackTextureLocation << std::endl;
        }

        // Bind texture resolutions to uniforms
#ifdef _WIN32
        float bufferResolution[2] = { (float)fb->width(), (float)fb->height()} ;
#else
        float bufferResolution[2] = { fb->float(width()), fb->float(height()) };
#endif

        //if (currentIteration)
        {
            for (int u = 0; u < resolutionSelfUniforms.size(); ++u)
            {
                std::string resolutionName = resolutionSelfUniforms.at(u) + tostr(u);
                ResolutionLocation = glGetUniformLocation(programID, resolutionName.c_str());
                glUniform2fv(ResolutionLocation, 1, bufferResolution);
            }
            //std::cout << "Uniforms set, outputbuffer w: " << fb->width() << std::endl;
        }
    }

    // 1rst attribute buffer : vertices
    glEnableVertexAttribArray(0);
    glBindBuffer(GL_ARRAY_BUFFER, vertexBuffer);
    glVertexAttribPointer(
        0, // The attribute we want to configure
        3,                  // size
        GL_FLOAT,           // type
        GL_FALSE,           // normalized?
        0,                  // stride
        (void*)0            // array buffer offset
    );

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Create and initialize render buffer (this must be moved out from draw altogether)
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    GLuint RenderFramebuffer = 0;
    glGenFramebuffers(1, &RenderFramebuffer);
    glBindFramebuffer(GL_FRAMEBUFFER, RenderFramebuffer);

    // The texture we're going to render to
    GLuint renderFramebufferTexture;
    glGenTextures(1, &renderFramebufferTexture);

    glBindTexture(GL_TEXTURE_2D, renderFramebufferTexture);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA32F, outputWidth, outputHeight, 0, GL_RGBA, GL_FLOAT, 0);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

    // Set texture as our colour attachement
    glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, renderFramebufferTexture, 0);

    // Set the list of draw buffers.
    GLenum RenderDrawBuffers[1] = { GL_COLOR_ATTACHMENT0 };
    glDrawBuffers(1, RenderDrawBuffers); // "1" is the size of DrawBuffers

    if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
    {
        logMessage("glCheckFramebufferStatus renderbuffer error");
        return false;
    }


    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Create and initialize final output buffer (this must be moved out from draw altogether)
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    GLuint FinalFramebuffer = 0;
    glGenFramebuffers(1, &FinalFramebuffer);
    glBindFramebuffer(GL_FRAMEBUFFER, FinalFramebuffer);

    // The texture we're going to render to
    GLuint FinalFramebufferTexture;
    glGenTextures(1, &FinalFramebufferTexture);

    glBindTexture(GL_TEXTURE_2D, FinalFramebufferTexture);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA32F, outputWidth, outputHeight, 0, GL_RGBA, GL_FLOAT, 0);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

    // Set texture as our colour attachement
    glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, FinalFramebufferTexture, 0);

    // Set the list of draw buffers.
    GLenum finalDrawBuffers[1] = { GL_COLOR_ATTACHMENT0 };
    glDrawBuffers(1, finalDrawBuffers);

    if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
    {
        logMessage("glCheckFramebufferStatus finalbuffer error");
        return false;
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Bind final buffer as draw buffer and draw. We copy this to intermediate only if we need to loop
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    glBindFramebuffer(GL_DRAW_FRAMEBUFFER, FinalFramebuffer);

    glViewport(0, 0, outputFrameBuffer->width(), outputFrameBuffer->height()); //
    glDrawArrays(GL_TRIANGLE_FAN, 0, 4);

    // If we do more than one pass:
    if (numIterations > 1)
    {
        // We need to set render result into intermediate only if we loop
        // Set renderbuffer as Read buffer, output buffer as Draw buffer and blit from renderbuffer to output
        glBindFramebuffer(GL_READ_FRAMEBUFFER, FinalFramebuffer);
        glBindFramebuffer(GL_DRAW_FRAMEBUFFER, RenderFramebuffer);
        glBlitFramebuffer(0, 0, outputWidth, outputHeight, 0, 0, outputWidth, outputHeight, GL_COLOR_BUFFER_BIT, GL_LINEAR);

        for (int c = 0; c < (numIterations - 1); ++c)
        {
        // Currently we have rendered stuff in output buffer

        // Set output buffer texture as uniform for shader
        for (int i = 0; i < feedbackTextureUniforms.size(); ++i)
        {
            std::string uniformName = feedbackTextureUniforms.at(i);
            feedbackTextureLocation = glGetUniformLocation(programID, uniformName.c_str());
            glActiveTexture(GL_TEXTURE0);
            glBindTexture(GL_TEXTURE_2D, FinalFramebufferTexture);
            glUniform1i(feedbackTextureLocation, 0);
        }

        // Set current iteration uniform
        for (int i = 0; i < iterUniforms.size(); ++i)
        {
            std::string uniformName = iterUniforms.at(i);
            generalLocation = glGetUniformLocation(programID, uniformName.c_str());
            glUniform1f(generalLocation, c + 1);
        }

        // Bind render buffer as draw buffer and draw using glViewport
        glBindFramebuffer(GL_DRAW_FRAMEBUFFER, RenderFramebuffer);

        glViewport(0, 0, outputFrameBuffer->width(), outputFrameBuffer->height()); //
        glDrawArrays(GL_TRIANGLE_FAN, 0, 4);

        // Set renderbuffer as Read buffer, output buffer as Draw buffer and blit from renderbuffer to output
        glBindFramebuffer(GL_READ_FRAMEBUFFER, RenderFramebuffer);
        glBindFramebuffer(GL_DRAW_FRAMEBUFFER, FinalFramebuffer);
        glBlitFramebuffer(0, 0, outputWidth, outputHeight, 0, 0, outputWidth, outputHeight, GL_COLOR_BUFFER_BIT, GL_LINEAR);

        // Now we have new result in output buffer texture and we can loop again
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Copy from outputbuffer to output array and do Nuke stuff
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    // Bind output buffer texture
    glBindTexture(GL_TEXTURE_2D, FinalFramebufferTexture);

    // Copy final render texture to float buffer which is used as node output
    float* outputBuffer = outputFrameBuffer->packedDataPointer(outputFrameBuffer->size(), 4);
    glGetTexImage (GL_TEXTURE_2D, 0, GL_RGBA, GL_FLOAT, outputBuffer);

    glBindTexture(GL_TEXTURE_2D, 0);

    glDisableVertexAttribArray(0);

    // Swap buffers. Not sure this is necessary
    glfwSwapBuffers(window);
    glfwPollEvents();

    glfwMakeContextCurrent(NULL);

    return true;
}



void NukeGLSL::engine (int y, int x, int r, DD::Image::ChannelMask channels, DD::Image::Row& row)
{
    // /////////////////////////////////////////////
    // Actual workload
    // /////////////////////////////////////////////

    if (outputFrameBuffer)
    {
        int size = outputFrameBuffer->size() * 4;
        int width = outputFrameBuffer->width();
        float* dataPtr = outputFrameBuffer->packedData();

        // Copy data from imagebuffer
        foreach ( z, channels )
        {
            if (z > 0 && z < 5)
            {
                float* outptr = row.writable(z) + x;
                for( int cur = x ; cur < r; cur++ )
                {
                    int offset = (y * width + cur) * 4;
                    if (offset > size) offset = 0;
                    *outptr++ = dataPtr[offset + z - 1];
                }
            }
        }
    } else {
        // Normal interpolation from input data, set to gray for debugging
        foreach ( z, channels )
        {
            float* outptr = row.writable(z) + x;
            for( int cur = x ; cur < r; cur++ )
            {
                float value = (int)cur % 2;
                *outptr++ = value;
            }
        }
    }
}


static DD::Image::Iop* build(Node* node)
{
    return (new DD::Image::NukeWrapper(new NukeGLSL(node)))->channelsRGBoptionalAlpha();
}

const DD::Image::Op::Description NukeGLSL::description(CLASS, "Image/Filter/NukeGLSL", build);

