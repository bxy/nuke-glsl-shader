#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <iostream>
#include <vector>

class FrameBufferLayer
{

public:
    FrameBufferLayer();
    ~FrameBufferLayer();

    void setSize(int width, int height);
    void setChannel(int channel);
    void setName(std::string name);
    void setData(float* data, int size);

    bool initialized()  { return _initialized; }
    std::string name()  { return _name; }
    int width()         { return _width; }
    int height()        { return _height; }
    int channel()       { return _channel; }
    float* data()       { return _data; }
    int size();
    int allocatedSize() { return _allocatedSize; }

    void initialize(int width, int height);
    void deinitialize();
    void allocateMemory();
    void deleteMemory();

private:
    bool _initialized;
    std::string _name;
    int _channel;
    int _width;
    int _height;
    int _allocatedSize;
    float* _data;
};
