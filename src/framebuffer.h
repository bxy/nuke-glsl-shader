#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <iostream>
#include <vector>

// OpenGL related includes
#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include "DDImage/DDImage_API.h"

class FrameBufferLayer;


class FrameBuffer
{

public:
    FrameBuffer();
    ~FrameBuffer();

    void initialize(int width, int height);

    bool initialized()  { return _initialized; }
    std::string name()  { return _name; }
    int width()         { return _width; }
    int height()        { return _height; }
    U64 hash()          { return _hash; }
    int numLayers()     { return _layers.size(); }
    int size();
    std::vector<FrameBufferLayer*> layers() { return _layers; }
    FrameBufferLayer* layer(int position);
    GLuint texture()    { return textureID; }

    void setInitialized(bool initState) { _initialized = initState; }
    void setName(std::string name) { _name = name;}
    void setHash(U64 hash) { _hash = hash; }
    void setSize(int width, int height);
    void setContext(GLFWwindow* context);
    void setLayerCount(int numLayers);
    void addLayer(FrameBufferLayer* layer, int position = -1);

    void deleteLayer(int position = -1);
    void deleteAll();

    void createTexture();
    void copyDataToGPU();
    float* packedData();
    float* createPackedData(int channels, int stride);
    float* packedDataPointer(int size, int channels);

private:
    bool _initialized;
    std::string _name;
    U64 _hash;
    int _width;
    int _height;
    float* _packedDataPointer;
    std::vector<FrameBufferLayer*> _layers;

    GLFWwindow* window;
    GLuint textureID;
};
