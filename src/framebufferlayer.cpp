#include "framebufferlayer.h"

FrameBufferLayer::FrameBufferLayer()
{
    _data = nullptr;
    _allocatedSize = 0;
    _width = 0;
    _height = 0;
    _initialized = false;
    _channel = 0;
}


FrameBufferLayer::~FrameBufferLayer()
{

}


void FrameBufferLayer::initialize(int width, int height)
{
    deinitialize();

    if (width && height)
    {
        setSize(width, height);
        allocateMemory();
        _initialized = true;
    }
}


void FrameBufferLayer::deinitialize()
{
    deleteMemory();
    _initialized = false;
}


void FrameBufferLayer::allocateMemory()
{
    deleteMemory();
    _data = new float[size()];
    _allocatedSize = size();
}


void FrameBufferLayer::deleteMemory()
{
    if (_data)
        delete _data;

    _data = nullptr;
    _allocatedSize = 0;
}


int FrameBufferLayer::size()
{
    if (_width < 0 || _height < 0)
        return 0;

    return _width * _height;
}


void FrameBufferLayer::setSize(int width, int height)
{
    _width = _height = 0;
    if (width && height)
    {
        _width = width;
        _height = height;
    }
}


void FrameBufferLayer::setChannel(int channel)
{
    _channel = channel;
}


void FrameBufferLayer::setName(std::string name)
{
    _name = name;
}


void FrameBufferLayer::setData(float* data, int size)
{
    if (_data && _allocatedSize >= size)
        memcpy(_data, data, sizeof(float) * size);
}
