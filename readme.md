# GLSL fragment shader code for Nuke 10.5
Created by Hendrik Proosa.
Licence is: do whatever you want with it.

Contributors:
Michael De Caria

Should work in Nuke 10.5 and 11, currently tested in 10.5
Does not work in Nuke 10, due to some incompatibility in SDK


## Build

I use MSVC 2010 x64 compiler from Win7 dev kit.
Nuke SDK reference guide has detailed instructions for compilation and dependencies.

I currently include and link against:
- glfw-3.2.1.bin.WIN64 
- glew-2.0.0
- Nuke 10.5 ndk

There should be no additional external dependencies.


## Install

Copy the NukeGLSL.dll file to Nuke plugins dir and for
conveniece add it to node menu through meny.py file. If it is not
loaded through menu.py, update all plugins to get it to show up.


## What it does

Executes fragment shader code :) Copy shader code to text knob and see what happens!
File reading not working yet, will do that later (although easy).


## How to use

Current version is pretty buggy, there are occasional glitches on playback where buffers are
not entirely read and part of image is black or some parts of some channels are black.

Most of the shaders from GLSLSandbox.com should work without modifications,
if not, see console for shader compilation errors. Some shaders need typo fixes etc.
Shadertoy shaders need need a wrapper, look into glslsandbox shaders, there are many ported from Shadertoy and show proper wrapper.
Shadertoy iChannels should be replicable using separate shader nodes for each channel and piping them
into main shader using inputs. As iChannel inputs are connected to texture inputs so that iChannel1 points to iTexture1, they should work. Not tested though.


## Currently available uniforms

### General data

Use any of the following keywords to get data into shader

**Frame number**
- iFrame, Frame, frame

Float value. Use this to access frame number directly.

**Time**
- iTime, Time, time

Float value. Use framerate knob to speed up or slow down the animation. Most shaders use time,
not frame number directly. Value is frame/framerate.
 
**Mouse position**
- iMouse, Mouse, mouse

This is vec2 type, x and y coordinates in screen pixels. Use knob in UI to set values.

### Input dependent data
Add index for input separation, without index references first input.

For example: iTexture0, iTexture1, iTexture2

**Resolution**
- iResolution, Resolution, resolution

This is vec2 type, texture width and height in pixels.

**Resolution of output**
- iResolutionSelf, ResolutionSelf, resolutionself

This is vec2 type, output width and height in pixels, can be useful for feedback loops and other stuff

**Input texture RGBA data**
- iTexture, Texture, texture, iChannel

This is sampler2D type, with access to RGBA channels using UV lookup.
Input textures are read in their original size and inside shader lie within to UV
coordinates 0.0-1.0 range. To get absolute pixel positions or aspect, use resolution
uniform. Currently inputs are requested in their own BBox, but texture is read in 0,0,w,h box which is wrong in some cases. Will be fixed.

**Feedback loop texture RGBA data**
- iFeedback, Feedback, feedback

This is sampler2D type, with access to RGBA channels using UV lookup.
Input textures are read in their original size and inside shader lie within to UV
coordinates 0.0-1.0 range. To get absolute pixel positions or aspect, use iResolutionSelf
uniform.

**Special feedback uniforms for iteration number and count**
- iIteration, Iteration, iteration
- iIterationCount, IterationCount, iterationcount

Iteration gives the current iteration number (iterations start at 1!) and iterationcount gives total number of iterations.

## Interesting stuff to try

See example scripts in example_scripts folder.
V07 file demonstrates multiple input texture use

## Example code

### Feedback loop usage

Following code will tile input into itself so that first iteration will produce 2x2 tiling, second 4x4 of original and so on:

```
#version 130
uniform float iTime;
uniform vec2 iResolution;
uniform sampler2D iTexture0;

void main()
{
	vec2 UV = gl_FragCoord.xy / iResolution.xy * 2;
	float intpart;
	UV.x = modf(UV.x, intpart);
	UV.y = modf(UV.y, intpart);
	vec4 outColor = texture2D(iTexture0, UV);
	gl_FragColor = outColor;
};
```

On first glimpse there is no looping going on, so how does it tile? But as result is fed back into iTexture0 input,
querying it again will run the shader again over result of previous rendering. To get something to change we modify UV coordinates so that
they tile in 0.0-1.0 range (modf function returns fractional part of float) and then we simply sample input texture. If we do it again and again, we get lots of tiles.

Result of this shader looks like this: [Tile shader](http://www.kalderafx.com/vfx/nuke/images/nukeglsl_feedback_tiles_01.png)

Another example of feedback, 20x loop on lens distort: [Lens distortion feedback](http://www.kalderafx.com/vfx/nuke/images/nukeglsl_feedback_distort_01.png)


## Known bugs and other annoyances

Lots of stuff is buggy, messy and otherwise not great.

Stuff to do and fix:
 - [ ] File reading
 - [ ] RGBA channels are currently hardcoded in multiple places, using input with more channels might crash
 - [ ] Sometimes some part of image is rendered black (usually on playback)
 - [ ] Sometimes it glitches with different colors (usually on playback)
 - [ ] Automatically strip comment lines from shader code. Fixes occasional compilation errors caused by bad characters in comments
 
Ideas for version 2:
 - [x] Feed result back to itself for specified number of times. Whis allows iterative drawing, sorting and other interesting stuff.
 - [ ] Dynamic knobs generated based on uniform names in shader
 - [ ] Persistent inputs: if input node hash does not change, do not perform texture upload
 - [ ] Asynchronous CPU-GPU uploads-downloads using PBO-s
 - [ ] Performance mode where image data is not downloaded but drawn directly to viewer using OpenGL
 
## Changelog

### 9. july 2018
Feedback loop working! First texture input is updated with render result for extra-easy access. In addition there is iFeedback uniform for more explicit access.
### 3. july 2018
Additional minor fixes related to cross-platform compiling.
Inputs that are disconnected are not copied anymore, needed input cast to DD::Image::Black* because disconnected inputs are given the special Black Op type as source. 
### 5. may 2018
Minor fixes for compiling on Linux submitted by Michael De Caria
### 18. april 2018
Fixed dumb mistake that prevented multi-texture inputs.